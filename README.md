# knowmac

Not the whole GATE-KNOWMAC project.

The true GATE-KNOWMAK project is tracked with two SVN repositories:
in the `sale` SVN repo at `internal/projects/knowmak`;
and, in the `gate-extras` repo at `knowmak`.

This gitlab repo exists so that we can use the Docker image repository.
